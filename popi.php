<?php

/**
* @author Paul Peter (paulpeter460@gmail.com) +255 719 77 55 22
* @version v1.0-2017

* This API is usefull to convert number into words 

* The function used is
	numberToWords();

* Class called popi

* To call the function
	$popi->numberToWords(value);

* Example
	
	--CODE--
	<?php
	include "popi.php";  // include this file is mandatory

	$number = 200; // your number to convert into words

	echo $popi->numberToWords($number);  // call the function from the class

	?>

	-- OUTPUT--
	mia mbili
*/
class Popi
{
		
	var $onesArray;
	var $tensArray;
	
	function __construct()
	{
		
	
	}

	private function onesFunction($number = null)
	{
		$onesArray = array();
		$onesArray['0'] = "sifuri";
		$onesArray['1'] = "moja";
		$onesArray['2'] = "mbili";
		$onesArray['3'] = "tatu";
		$onesArray['4'] = "nne";
		$onesArray['5'] = "tano";
		$onesArray['6'] = "sita";
		$onesArray['7'] = "saba";
		$onesArray['8'] = "nane";
		$onesArray['9'] = "tisa";

		return $onesArray[$number];
	}

	private function tensFunction($number = null)
	{
		$tensArray = array();
		$tensArray['1'] = "kumi";
		$tensArray['2'] = "ishirini";
		$tensArray['3'] = "thelathini";
		$tensArray['4'] = "arobaini";
		$tensArray['5'] = "hamsini";
		$tensArray['6'] = "sitini";
		$tensArray['7'] = "sabini";
		$tensArray['8'] = "themanini";
		$tensArray['9'] = "tisini";

		return $tensArray[$number];
	}

	public function numberToWords($number = null) // function to convert number to words
	{
		if (is_numeric($number))
		{
			$number = (int)$number;
			if($number > 2000000000)
			{
				return $this->popiError("Number exceeds limit");
				exit();
			}
			return $this->convertNumberToWords($number);
		}

		else
		{
			return $number;
		}
	}

	private function convertNumberToWords($number)
	{
		$number_lengh = strlen($number);

		switch ($number_lengh) {
			case '1':
				return $this->onesNumber($number);
				break;

			case '2':
				return $this->tensNumber($number);
				break;

			case '3':
				return $this->hundredsNumber($number);
				break;

			case '4':
				return $this->thousandsNumber($number);
				break;

			case '5':
				return $this->tenthousandsNumber($number);
				break;

			case '6':
				return $this->hundredthousandsNumber($number);
				break;

			case '7':
				return $this->millionsNumber($number);
				break;

			case '8':
				return $this->tenmillionsNumber($number);
				break;

			case '9':
				return $this->hundredmillionsNumber($number);
				break;

			case '10':
				return $this->billionsNumber($number);
				break;

			default:
				# code...
				return $this->popiError("Number exceeds limit");
				break;
		}
	}

	private function onesNumber($number)
	{
		return $this->onesFunction($number);
	}

	private function tensNumber($number)
	{
		$number = str_split($number);
		if ($number[1] == 0) {
			$onesNumber = " ";
		}

		else
		{
			$onesNumber = "na ".$this->onesFunction($number[1]);
		}
		
		$tensNumber = $this->tensFunction($number[0]);

		return $tensNumber." ".$onesNumber;
	}

	private function hundredsNumber($number)
	{
		$number_split = str_split($number);
		$firstNumber = "mia ".$this->onesFunction($number_split[0]);

		if(substr($number, 1) == 0)
		{
			$otherNumber = "";
		}

		else
		{
			$otherNumber = " na ".$this->numberToWords(substr($number, 1));
		}

		return $firstNumber.$otherNumber;


	}


	private function thousandsNumber($number)
	{
		$number_split = str_split($number);
		$firstNumber = "elfu ".$this->onesFunction($number_split[0]);

		if(substr($number, 1) == 0)
		{
			$otherNumber = "";
		}

		else
		{
			$otherNumber = " na ".$this->numberToWords(substr($number, 1));
		}

		return $firstNumber.$otherNumber;


	}


	private function tenthousandsNumber($number)
	{
		$number_split = str_split($number);
		$firstNumber = $this->tensNumber($number_split[0].$number_split[1])." elfu ";

		if(substr($number, 2) == 0)
		{
			$otherNumber = "";
		}

		else
		{
			$otherNumber = " na ".$this->numberToWords(substr($number, 2));
		}

		return $firstNumber.$otherNumber;
	}


	private function hundredthousandsNumber($number)
	{
		$number_split = str_split($number);
		$firstNumber = "laki ".$this->onesNumber($number_split[0]);

		if(substr($number, 1) == 0)
		{
			$otherNumber = "";
		}

		else
		{
			$otherNumber = " na ".$this->numberToWords(substr($number, 1));
		}

		return $firstNumber.$otherNumber;
	}

	private function millionsNumber($number)
	{
		$number_split = str_split($number);
		$firstNumber = "milioni ".$this->onesNumber($number_split[0]);

		if(substr($number, 1) == 0)
		{
			$otherNumber = "";
		}

		else
		{
			$otherNumber = " na ".$this->numberToWords(substr($number, 1));
		}

		return $firstNumber.$otherNumber;
	}

	private function tenmillionsNumber($number)
	{
		$number_split = str_split($number);
		$firstNumber = "milioni ".$this->tensNumber($number_split[0].$number_split[1]);

		if(substr($number, 2) == 0)
		{
			$otherNumber = "";
		}

		else
		{
			$otherNumber = " na ".$this->numberToWords(substr($number, 2));
		}

		return $firstNumber.$otherNumber;
	}

	private function hundredmillionsNumber($number)
	{
		$number_split = str_split($number);
		$firstNumber = "milioni ".$this->hundredsNumber($number_split[0].$number_split[1].$number_split[2]);

		if(substr($number, 3) == 0)
		{
			$otherNumber = "";
		}

		else
		{
			$otherNumber = " na ".$this->numberToWords(substr($number, 3));
		}

		return $firstNumber.$otherNumber;
	}

	private function billionsNumber($number)
	{
		$number_split = str_split($number);
		$firstNumber = "bilioni ".$this->onesNumber($number_split[0]);

		if(substr($number, 1) == 0)
		{
			$otherNumber = "";
		}

		else
		{
			$otherNumber = " na ".$this->numberToWords(substr($number, 1));
		}

		return $firstNumber.$otherNumber;
	}



	private function popiError($error_msg)
	{
		$msg = '
				<div style="
					position: fixed;
					top: 0;
					z-index: 100;
					width: 100%;
					margin:auto;
					color: red;
					border: 1px solid red;
					border-radius: 4px;
					padding: 16px 8px 16px 8px;
					font-size: 16px;
					font-family: Segoe UI Semibold, Arial;
					background-color:rgba(255,0,0,0.2);
					"><center>error: '.$error_msg.' @popi class : function $popi->numberToWords()</center>
				</div>
		';
		return $msg;
	}

}

$popi = new Popi;



?>