Procedure:

Step-1: Download the file called ' popi.ph '

Step-2: Copy the file into the same directory of your project you are working on

Step-3: In your php file of your project, include my API file
		
		include 'popi.php';

Step-4: whenever you want to convert number into words just call the function
		
		$popi->numberToWords(value);



		** EXAMPLE **

		--CODE--
	<?php
	include "popi.php";  // include this file is mandatory

	$number = 200; // your number to convert into words

	echo $popi->numberToWords($number);  // call the function from the class

	?>

	-- OUTPUT--
	mia mbili